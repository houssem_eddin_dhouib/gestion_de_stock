package com.stock.services;

import java.util.List;

import com.stock.entites.Client;

public interface IClientService {

	public Client save(Client entity);
	public Client update (Client entity);
	public Client delete(Long id);
	public Client getById(Long id);
	public List<Client> selectAll();
	public List<Client> selectAll(String sortField,String sort);
	public Client findOne(String pramName,Object paramValue);
	public Client findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
