package com.stock.services;

import java.util.List;

import com.stock.entites.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurService {

	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity);
	public LigneCommandeFournisseur update (LigneCommandeFournisseur entity);
	public LigneCommandeFournisseur delete(Long id);
	public LigneCommandeFournisseur getById(Long id);
	public List<LigneCommandeFournisseur> selectAll();
	public List<LigneCommandeFournisseur> selectAll(String sortField,String sort);
	public LigneCommandeFournisseur findOne(String pramName,Object paramValue);
	public LigneCommandeFournisseur findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
