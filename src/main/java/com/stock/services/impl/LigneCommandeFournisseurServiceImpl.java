package com.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.dao.ILigneCommandeFournisseurDao;
import com.stock.entites.LigneCommandeFournisseur;
import com.stock.services.ILigneCommandeFournisseurService;

@Transactional
public class LigneCommandeFournisseurServiceImpl implements ILigneCommandeFournisseurService{

	private ILigneCommandeFournisseurDao dao;
	
	
	public void setDao(ILigneCommandeFournisseurDao dao) {
		this.dao = dao;
	}

	@Override
	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public LigneCommandeFournisseur delete(Long id) {
		// TODO Auto-generated method stub
		return dao.delete(id);
	}

	@Override
	public LigneCommandeFournisseur getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeFournisseur findOne(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public LigneCommandeFournisseur findOne(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

}
