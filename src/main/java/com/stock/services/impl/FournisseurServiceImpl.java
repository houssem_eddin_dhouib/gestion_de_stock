package com.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.dao.IFournisseurDAO;
import com.stock.entites.Fournisseur;
import com.stock.services.IFournisseurService;

@Transactional
public class FournisseurServiceImpl implements IFournisseurService{

	private IFournisseurDAO dao;
	
	
	public void setDao(IFournisseurDAO dao) {
		this.dao = dao;
	}

	@Override
	public Fournisseur save(Fournisseur entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Fournisseur update(Fournisseur entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public Fournisseur delete(Long id) {
		// TODO Auto-generated method stub
		return dao.delete(id);
	}

	@Override
	public Fournisseur getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public List<Fournisseur> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Fournisseur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Fournisseur findOne(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public Fournisseur findOne(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

}
