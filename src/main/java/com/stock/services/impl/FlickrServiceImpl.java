package com.stock.services.impl;

import java.io.InputStream;

import com.stock.dao.IFlickrDao;
import com.stock.services.IFlickrService;

public class FlickrServiceImpl implements IFlickrService {
 private IFlickrDao dao;
 public void setDao(IFlickrDao dao) {
	this.dao = dao;
}
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		// TODO Auto-generated method stub
		return dao.savePhoto(photo, title);
	}

}
