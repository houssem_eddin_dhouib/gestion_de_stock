package com.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.dao.ICommandeFournisseurDao;
import com.stock.entites.CommandeFournisseur;
import com.stock.services.ICommandeFournisseurService;

@Transactional
public class CommandeFournisseurServiceImpl implements ICommandeFournisseurService{

	private ICommandeFournisseurDao dao;
	
	
	public void setDao(ICommandeFournisseurDao dao) {
		this.dao = dao;
	}

	@Override
	public CommandeFournisseur save(CommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public CommandeFournisseur update(CommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public CommandeFournisseur delete(Long id) {
		// TODO Auto-generated method stub
		return dao.delete(id);
	}

	@Override
	public CommandeFournisseur getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public List<CommandeFournisseur> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<CommandeFournisseur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public CommandeFournisseur findOne(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public CommandeFournisseur findOne(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

}
