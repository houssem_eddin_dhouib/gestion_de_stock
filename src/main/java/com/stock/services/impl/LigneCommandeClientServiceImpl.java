package com.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.dao.ILigneCommandeClientDao;
import com.stock.entites.LigneCommandeClient;
import com.stock.services.ILigneCommandeClientService;

@Transactional
public class LigneCommandeClientServiceImpl implements ILigneCommandeClientService{

	private ILigneCommandeClientDao dao;
	
	
	public void setDao(ILigneCommandeClientDao dao) {
		this.dao = dao;
	}

	@Override
	public LigneCommandeClient save(LigneCommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public LigneCommandeClient update(LigneCommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public LigneCommandeClient delete(Long id) {
		// TODO Auto-generated method stub
		return dao.delete(id);
	}

	@Override
	public LigneCommandeClient getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public List<LigneCommandeClient> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<LigneCommandeClient> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeClient findOne(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public LigneCommandeClient findOne(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

}
