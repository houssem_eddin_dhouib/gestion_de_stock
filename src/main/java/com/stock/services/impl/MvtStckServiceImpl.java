package com.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.dao.IMvtStckDao;
import com.stock.entites.MvtStck;
import com.stock.services.IMvtStckService;

@Transactional
public class MvtStckServiceImpl implements IMvtStckService{

	private IMvtStckDao dao;

	@Override
	public MvtStck save(MvtStck entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public MvtStck update(MvtStck entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public MvtStck delete(Long id) {
		// TODO Auto-generated method stub
		return dao.delete(id);
	}

	@Override
	public MvtStck getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public List<MvtStck> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<MvtStck> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public MvtStck findOne(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public MvtStck findOne(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String pramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

	@Override
	public Long findCountBy(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(pramName, paramValue);
	}

	public void setDao(IMvtStckDao dao) {
		this.dao = dao;
	}
	
	

}
