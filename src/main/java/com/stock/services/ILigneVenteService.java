package com.stock.services;

import java.util.List;

import com.stock.entites.LigneVente;

public interface ILigneVenteService {

	public LigneVente save(LigneVente entity);
	public LigneVente update (LigneVente entity);
	public LigneVente delete(Long id);
	public LigneVente getById(Long id);
	public List<LigneVente> selectAll();
	public List<LigneVente> selectAll(String sortField,String sort);
	public LigneVente findOne(String pramName,Object paramValue);
	public LigneVente findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
