package com.stock.services;

import java.util.List;

import com.stock.entites.Fournisseur;

public interface IFournisseurService {

	public Fournisseur save(Fournisseur entity);
	public Fournisseur update (Fournisseur entity);
	public Fournisseur delete(Long id);
	public Fournisseur getById(Long id);
	public List<Fournisseur> selectAll();
	public List<Fournisseur> selectAll(String sortField,String sort);
	public Fournisseur findOne(String pramName,Object paramValue);
	public Fournisseur findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
