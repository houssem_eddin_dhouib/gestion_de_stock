package com.stock.services;

import java.util.List;

import com.stock.entites.MvtStck;

public interface IMvtStckService {

	public MvtStck save(MvtStck entity);
	public MvtStck update (MvtStck entity);
	public MvtStck delete(Long id);
	public MvtStck getById(Long id);
	public List<MvtStck> selectAll();
	public List<MvtStck> selectAll(String sortField,String sort);
	public MvtStck findOne(String pramName,Object paramValue);
	public MvtStck findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
