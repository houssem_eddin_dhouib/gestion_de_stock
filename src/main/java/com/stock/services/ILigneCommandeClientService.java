package com.stock.services;

import java.util.List;

import com.stock.entites.LigneCommandeClient;

public interface ILigneCommandeClientService {

	public LigneCommandeClient save(LigneCommandeClient entity);
	public LigneCommandeClient update (LigneCommandeClient entity);
	public LigneCommandeClient delete(Long id);
	public LigneCommandeClient getById(Long id);
	public List<LigneCommandeClient> selectAll();
	public List<LigneCommandeClient> selectAll(String sortField,String sort);
	public LigneCommandeClient findOne(String pramName,Object paramValue);
	public LigneCommandeClient findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
