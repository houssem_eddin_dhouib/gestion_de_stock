package com.stock.services;

import java.util.List;

import com.stock.entites.CommandeFournisseur;

public interface ICommandeFournisseurService {

	public CommandeFournisseur save(CommandeFournisseur entity);
	public CommandeFournisseur update (CommandeFournisseur entity);
	public CommandeFournisseur delete(Long id);
	public CommandeFournisseur getById(Long id);
	public List<CommandeFournisseur> selectAll();
	public List<CommandeFournisseur> selectAll(String sortField,String sort);
	public CommandeFournisseur findOne(String pramName,Object paramValue);
	public CommandeFournisseur findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
