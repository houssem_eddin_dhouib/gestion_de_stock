package com.stock.services;

import java.util.List;

import com.stock.entites.Utilisateur;

public interface IUtilisateurService {

	public Utilisateur save(Utilisateur entity);
	public Utilisateur update (Utilisateur entity);
	public Utilisateur delete(Long id);
	public Utilisateur getById(Long id);
	public List<Utilisateur> selectAll();
	public List<Utilisateur> selectAll(String sortField,String sort);
	public Utilisateur findOne(String pramName,Object paramValue);
	public Utilisateur findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
