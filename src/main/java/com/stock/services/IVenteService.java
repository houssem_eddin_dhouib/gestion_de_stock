package com.stock.services;

import java.util.List;

import com.stock.entites.Vente;

public interface IVenteService {

	public Vente save(Vente entity);
	public Vente update (Vente entity);
	public Vente delete(Long id);
	public Vente getById(Long id);
	public List<Vente> selectAll();
	public List<Vente> selectAll(String sortField,String sort);
	public Vente findOne(String pramName,Object paramValue);
	public Vente findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
