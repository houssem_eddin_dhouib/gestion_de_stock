package com.stock.services;

import java.util.List;

import com.stock.entites.CommandeClient;

public interface ICommandeClientService {

	public CommandeClient save(CommandeClient entity);
	public CommandeClient update (CommandeClient entity);
	public CommandeClient delete(Long id);
	public CommandeClient getById(Long id);
	public List<CommandeClient> selectAll();
	public List<CommandeClient> selectAll(String sortField,String sort);
	public CommandeClient findOne(String pramName,Object paramValue);
	public CommandeClient findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
