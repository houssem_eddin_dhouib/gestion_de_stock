package com.stock.services;

import java.util.List;

import com.stock.entites.Category;

public interface ICategoryService {

	public Category save(Category entity);
	public Category update (Category entity);
	public Category delete(Long id);
	public Category getById(Long id);
	public List<Category> selectAll();
	public List<Category> selectAll(String sortField,String sort);
	public Category findOne(String pramName,Object paramValue);
	public Category findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
