package com.stock.services;

import java.util.List;

import com.stock.entites.Article;

public interface IArticleService {

	public Article save(Article entity);
	public Article update (Article entity);
	public Article delete(Long id);
	public Article getById(Long id);
	public List<Article> selectAll();
	public List<Article> selectAll(String sortField,String sort);
	public Article findOne(String pramName,Object paramValue);
	public Article findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
}
