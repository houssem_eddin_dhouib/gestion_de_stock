package com.stock.dao;

import com.stock.entites.CommandeFournisseur;

public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur> {

}
