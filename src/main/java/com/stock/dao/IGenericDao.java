package com.stock.dao;

import java.util.List;

public interface IGenericDao<E> {
	public E save(E entity);
	public E update (E entity);
	public E delete(Long id);
	public E getById(Long id);
	public List<E> selectAll();
	public List<E> selectAll(String sortField,String sort);
	public E findOne(String pramName,Object paramValue);
	public E findOne(String[] pramName,Object[] paramValue);
	public Long findCountBy(String pramName,Object paramValue);
	public Long findCountBy(String[] pramName,Object[] paramValue);
	

}
