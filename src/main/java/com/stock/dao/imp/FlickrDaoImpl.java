package com.stock.dao.imp;

import java.io.InputStream;

import javax.swing.JOptionPane;

import org.scribe.model.Token;
import org.scribe.model.Verifier;
import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.stock.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao{

	private Flickr flickr;
	
	private UploadMetaData uploadMetaData =new  UploadMetaData();
	
	private String apiKey="8d0ef78041bdc81f8526a8679a5308e3";
	
	private String sharedSecret="e33658c0d3a6ddc6";
	
	
	private void connect() {
		flickr=new Flickr(apiKey, sharedSecret, new REST());
		Auth auth =new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157688248584210-eed3e8b197fe9d75");
		auth.setTokenSecret("8cec0a8e3adb18d1");
		RequestContext requestContext=RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		connect();
		uploadMetaData.setTitle(title);
		String photoId=flickr.getUploader().upload(photo, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}
	public void auth() {
		flickr=new Flickr(apiKey, sharedSecret, new REST());
		
		AuthInterface authInterface=flickr.getAuthInterface();
		
		Token token=authInterface.getRequestToken();
		
		System.out.println("token:"+token);
		
		String url=authInterface.getAuthorizationUrl(token, Permission.DELETE);
		
		System.out.println("Follow this url to follow:");
		System.out.println(url.toString());
		System.out.println("paste in token give you");
		System.out.println(">>");
		
		String tokenKey=JOptionPane.showInputDialog(null);
		
		Token requestToken =authInterface.getAccessToken(token,new Verifier(tokenKey));
		System.out.println("Auth success");
		
		Auth auth=null;
		try {
			auth=authInterface.checkToken(requestToken);
		}catch(FlickrException e)
		{
		e.printStackTrace();
		}
		System.out.println("Token"+requestToken.getToken());
		System.out.println("Secret"+requestToken.getSecret());
		System.out.println("nsid"+auth.getUser());
		System.out.println("Realname:"+auth.getUser().getRealName());
		System.out.println("UserName:"+auth.getUser().getUsername());
		System.out.println("PermissionType"+auth.getPermission().getType());
	}

}
