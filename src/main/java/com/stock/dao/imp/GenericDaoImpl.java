package com.stock.dao.imp;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.stock.dao.IGenericDao;
@SuppressWarnings("unchecked")
public class GenericDaoImpl<E> implements IGenericDao<E>{
	@PersistenceContext
	EntityManager em;
	
	private Class<E> type;
	
	
	public GenericDaoImpl() {
		Type t=getClass().getGenericSuperclass();
		ParameterizedType pt=(ParameterizedType) t;
		type =(Class<E>) pt.getActualTypeArguments()[0];
	}

	@Override
	public E save(E entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	public E update(E entity) {
		em.merge(entity);
		return entity;
	}

	@Override
	public E delete(Long id) {
		E object=em.find(type,id);
		em.remove(object);
		return object ;
	}

	@Override
	public E getById(Long id) {
		return em.find(type,id);
		
	}

	@Override
	public List<E> selectAll() {
		Query query=em.createQuery("select t from "+type.getSimpleName()+" t");
		return query.getResultList();
		}

	@Override
	public List<E> selectAll(String sortField, String sort) {
		Query query=em.createQuery("select t from "+type.getSimpleName()+" t "
				+ "order by "+sortField+" "+sort);
		return query.getResultList();
	}

	@Override
	public E findOne(String pramName, Object paramValue) {
		Query query=em.createQuery("select t from "+type.getSimpleName()+" t "
				+ "where "+pramName+" = :x");
		query.setParameter("x", paramValue);
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
	}

	@Override
	public E findOne(String[] pramName, Object[] paramValue) {
		String ch="select e from "+type.getSimpleName()+" e where ";
		for(int i=0;i<pramName.length;i++)
		{
			ch+=" e."+pramName[i]+" = :x"+i;
			if (i+1<pramName.length)
			{
				ch+=" and ";
			}
		}
		Query query=em.createQuery(ch);
		for(int i=0;i<pramName.length;i++)
		{
			query.setParameter("x"+i, paramValue[i]);
		}
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
	}

	@Override
	public Long findCountBy(String pramName, Object paramValue) {
		Query query=em.createQuery("select t from "+type.getSimpleName()+" t "
				+ "where "+pramName+" = :x");
		query.setParameter("x", paramValue);
		return (long) (query.getResultList().size() > 0 ? ((Long) query.getSingleResult()).intValue() : 0);
	}

	@Override
	public Long findCountBy(String[] pramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return null;
	}
	public Class<E> getType() {
		return type;
	}

}
