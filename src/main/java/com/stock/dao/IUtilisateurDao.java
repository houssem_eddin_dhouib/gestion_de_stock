package com.stock.dao;

import com.stock.entites.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {

}
