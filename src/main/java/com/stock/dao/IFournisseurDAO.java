package com.stock.dao;

import com.stock.entites.Fournisseur;

public interface IFournisseurDAO extends IGenericDao<Fournisseur> {

}
