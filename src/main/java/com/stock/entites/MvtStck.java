package com.stock.entites;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType; 

@Entity
@Table(name="mvt_stck")
public class MvtStck implements Serializable {
	public static final int ENTREE=1;
	public static final int SORTIE=2;
	@Id
	@GeneratedValue
	private Long idMvtStck;
	
	public MvtStck() {
		super();
	}
	public Long getIdMvtStck() {
		return idMvtStck;
	}
	public void setIdMvtStck(Long idMvtStck) {
		this.idMvtStck = idMvtStck;
	}
	public Date getDateMvtStck() {
		return dateMvtStck;
	}
	public void setDateMvtStck(Date dateMvtStck) {
		this.dateMvtStck = dateMvtStck;
	}
	public int getTypMvt() {
		return typMvt;
	}
	public void setTypMvt(int typMvt) {
		this.typMvt = typMvt;
	}
	public BigDecimal getQuantite() {
		return quantite;
	}
	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvtStck;
	private int typMvt;
	private BigDecimal quantite;
}
