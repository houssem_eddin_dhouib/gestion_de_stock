package com.stock.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType; 

@Entity
@Table(name="commande_fournisseur")
public class CommandeFournisseur implements Serializable{
	@Id
	@GeneratedValue
	private Long idCommandeFournisseur;
	
	@ManyToOne
	@JoinColumn(name="idFournisseur")
	private Fournisseur fournisseur;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommandeFournisseur;
	
	@OneToMany(mappedBy="commandeFournisseur")
	private List<LigneCommandeFournisseur> lignesCommande;

	public Long getIdCommandeFournisseur() {
		return idCommandeFournisseur;
	}

	public void setIdCommandeFournisseur(Long idCommandeFournisseur) {
		this.idCommandeFournisseur = idCommandeFournisseur;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public Date getDateCommandeFournisseur() {
		return dateCommandeFournisseur;
	}

	public void setDateCommandeFournisseur(Date dateCommandeFournisseur) {
		this.dateCommandeFournisseur = dateCommandeFournisseur;
	}

	public List<LigneCommandeFournisseur> getLignesCommande() {
		return lignesCommande;
	}

	public CommandeFournisseur() {
		super();
	}

	public void setLignesCommande(List<LigneCommandeFournisseur> lignesCommande) {
		this.lignesCommande = lignesCommande;
	}
	
}
